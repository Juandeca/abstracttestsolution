import {
	Box,
	AspectRatio,
	Image,
	Stack,
	Progress,
	Text,
	Badge,
	HStack,
	Checkbox,
} from "@chakra-ui/react";
import axios from "axios";
import { useState } from "react";

export default function PokemonData({
	setIsLoading,
	pokemon,
	catchedPokemon,
	getCatchedPokemon,
}) {
	const [catched, setCatched] = useState(
		catchedPokemon.find((pokemonInList) => pokemonInList.id === pokemon.id)
	);

	const changeCatchedPokemon = (catched) => {
		setIsLoading(true);
		const requestOptions = {
			method: catched ? "POST" : "DELETE",
			url: catched ? "/api/catched" : `/api/catched/${pokemon.id}`,
			data: catched
				? {
						id: pokemon.id,
						name: pokemon.name,
				  }
				: {},
		};

		axios(requestOptions)
			.catch((error) => {
				console.log(error);
			})
			.finally(() => {
				getCatchedPokemon();
				setIsLoading(false);
			});
	};

	return (
		<Stack spacing="5" pb="5">
			<Stack spacing="5" position="relative" align="center">
				<Box position="absolute" right="0" zIndex="99">
					<Checkbox
						onChange={(e) => {
							setCatched(!catched);
							changeCatchedPokemon(e.target.checked);
						}}
						isChecked={catched}
					>
						Catched
					</Checkbox>
				</Box>
				<AspectRatio w="full" ratio={1}>
					<Image
						objectFit="contain"
						src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/${pokemon.id}.png`}
					/>
				</AspectRatio>
				<Stack direction="row" spacing="5" marginTop="50px">
					<Stack>
						<Text fontSize="sm">Weight</Text>
						<Text>{pokemon.weight}</Text>
					</Stack>
					<Stack>
						<Text fontSize="sm">Height</Text>
						<Text>{pokemon.height}</Text>
					</Stack>
					<Stack>
						<Text fontSize="sm">Movimientos</Text>
						<Text>{pokemon.moves.length}</Text>
					</Stack>
					<Stack>
						<Text fontSize="sm">Tipos</Text>
						<HStack>
							{pokemon.types.map((type, i) => (
								<Badge key={i}>{type.type.name}</Badge>
							))}
						</HStack>
					</Stack>
				</Stack>
			</Stack>

			<Stack spacing="5" p="5" bg="gray.100" borderRadius="xl">
				<Stack>
					<Text fontSize="xs">hp</Text>
					<Progress
						bg="gray.300"
						borderRadius="full"
						value={pokemon.stats[0].base_stat}
					/>
				</Stack>
				<Stack>
					<Text fontSize="xs">attack</Text>
					<Progress
						bg="gray.300"
						borderRadius="full"
						value={pokemon.stats[1].base_stat}
					/>
				</Stack>
			</Stack>
		</Stack>
	);
}
