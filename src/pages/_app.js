import "@/styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import {
	Container,
	Stack,
	CircularProgress,
	Button,
	SimpleGrid,
	Flex,
	Box,
	Modal,
	ModalOverlay,
	ModalHeader,
	ModalBody,
	ModalContent,
	ModalCloseButton,
	Tabs,
	TabList,
	Tab,
	TabPanels,
	TabPanel,
	useDisclosure,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

export default function App({ Component, pageProps }) {
	const [mostrarCapturados, setMostrarCapturados] = useState(false);

	return (
		<ChakraProvider>
			<Box
				position="fixed"
				borderBottom="2px solid #EDF2F7"
				background="white"
				width="100%"
				zIndex="1"
				display="flex"
				justifyContent="center"
			>
				<Button
					borderBottom={
						!mostrarCapturados ? "1px solid blue" : "1px solid white"
					}
					borderRadius="0px"
					background="white"
					onClick={() => setMostrarCapturados(false)}
				>
					Lista Completa
				</Button>
				<Button
					borderBottom={
						mostrarCapturados ? "1px solid blue" : "1px solid white"
					}
					borderRadius="0px"
					background="white"
					onClick={() => setMostrarCapturados(true)}
				>
					Capturados
				</Button>
			</Box>
			<Component {...pageProps} mostrarCapturados={mostrarCapturados} />
		</ChakraProvider>
	);
}
